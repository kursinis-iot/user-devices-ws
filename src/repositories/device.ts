import {PrismaClient} from '@prisma/client'

import {deleteDeviceUsers, upsertDeviceUser} from "./deviceUser.js";

export async function upsertDevice(
    prisma: PrismaClient,
    uuid: string,
) {
    await prisma.device.upsert({
        where: {
            uuid: uuid
        },
        update: {},
        create: {
            uuid: uuid
        }
    })
}

export type UserData = {
    uuid: string,
    role: string
}

export async function upsertDeviceWithUsers(
    prisma: PrismaClient,
    uuid: string,
    userData: UserData[],
    userUuidsToUnlink: string[]
) {
    await prisma.$transaction(async (prisma) => {
        await upsertDevice(prisma as PrismaClient, uuid);

        await Promise.all(userData.map(async (user) => upsertDeviceUser(
            prisma as PrismaClient,
            uuid,
            user.uuid,
            user.role
        )))

        await deleteDeviceUsers(prisma as PrismaClient, uuid, userUuidsToUnlink);
    });
}

export async function deleteDevice(
    prisma: PrismaClient,
    uuid: string
) {
    return prisma.device.delete({
        where: {
            uuid: uuid
        }
    });
}