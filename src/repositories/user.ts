import {PrismaClient} from '@prisma/client'

export async function upsertUser(
    prisma: PrismaClient,
    uuid: string,
    email: string,
    firstName: string,
    lastName: string,
    language: string
) {
    return prisma.user.upsert({
        where: {
            uuid: uuid
        },
        update: {
            email: email,
            firstName: firstName,
            lastName: lastName,
            language: language,
        },
        create: {
            uuid: uuid,
            email: email,
            firstName: firstName,
            lastName: lastName,
            language: language,
        }
    });
}

export async function deleteUser(
    prisma: PrismaClient,
    uuid: string,
) {
    return prisma.user.delete({
        where: {
            uuid: uuid
        }
    });
}