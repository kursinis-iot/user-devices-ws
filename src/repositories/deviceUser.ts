import {PrismaClient} from '@prisma/client'

export async function upsertDeviceUser(
    prisma: PrismaClient,
    deviceUuid: string,
    userUuid: string,
    role: string
) {
    await prisma.deviceUser.upsert({
        where: {
            deviceUuid_userUuid: {
                deviceUuid: deviceUuid,
                userUuid: userUuid
            }
        },
        update: {
            role: role
        },
        create: {
            deviceUuid: deviceUuid,
            userUuid: userUuid,
            role: role
        }
    });
}

export async function deleteDeviceUsers(
    prisma: PrismaClient,
    deviceUuid: string,
    userUuids: string[],
) {
    return prisma.deviceUser.deleteMany({
        where: {
            deviceUuid: deviceUuid,
            userUuid: {
                in: userUuids
            }
        }
    });
}

export async function doesDeviceBelongToUser(
    prisma: PrismaClient,
    deviceUuid: string,
    userUuid: string
) {
    const deviceUser = await prisma.deviceUser.findFirst({
        where: {
            deviceUuid: deviceUuid,
            userUuid: userUuid
        }
    });

    return Boolean(deviceUser);
}