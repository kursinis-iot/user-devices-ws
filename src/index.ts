import http, {Server} from "http";
import {connection, request, server as WebSocketServer} from "websocket";
import {PrismaClient} from "@prisma/client";
import {randomBytes} from "node:crypto";
import {EachMessagePayload} from "kafkajs";

import kafka from "./services/kafka/kafka.js";
import producer from "./services/kafka/producer.js";
import consumer from "./services/kafka/consumer.js";
import {
    calcMillisBeforeTokenExpires,
    verifyToken
} from "./services/user-authenticator/index.js";
import {doesDeviceBelongToUser} from "./repositories/deviceUser.js";
import {markDeviceConnected, markDeviceDisconnected} from "./services/redis/device.js";
import {consumeDeviceAction, DisconnectDeviceAction} from "./services/redis/deviceActions.js";
import {markUserConnected, markUserDisconnected} from "./services/redis/users.js";
import {markDeviceAnomaliesConnected, markDeviceAnomaliesDisconnected} from "./services/redis/deviceAnomalies.js";
import moment from "moment";

type OpenConnection = {
    userUuid: string,
    connection: connection,
    guardTimeout: NodeJS.Timeout,
    subscribedDeviceSensors: string[],
    subscribedDeviceAnomalies: string[]
};

const poolActionsEveryMs = 100;

const podName = process.env.POD_NAME ?? '';
const appUrl = process.env.APP_URL ?? '';
const deviceSensorSubscriptionTopic = process.env.KAFKA_DEVICE_SENSOR_SUBSCRIBE_TOPIC ?? '';
const deviceAnomaliesSubscriptionTopic = process.env.KAFKA_DEVICE_ANOMALIES_SUBSCRIBE_TOPIC ?? '';
const deviceSensorTopic = (process.env.KAFKA_DEVICE_SENSOR_TOPIC_PREFIX ?? '') + podName;
const deviceAnomaliesTopic = (process.env.KAFKA_DEVICE_ANOMALIES_TOPIC_PREFIX ?? '') + podName;

const openConnections: Record<string, OpenConnection> = {};
const subscribedDeviceSensors: Record<string, string[]> = {};
const subscribedDeviceAnomalies: Record<string, string[]> = {};

function generateNewConnectionId(): string {
    return randomBytes(16).toString('hex');
}

function getConnection(connectionId: string): OpenConnection | undefined {
    return openConnections[connectionId];
}

function hasConnectionSubscribedDeviceSensors(connectionId: string, deviceUuid: string): boolean {
    const connection = getConnection(connectionId);
    if (connection === undefined)
        return false;

    return connection.subscribedDeviceSensors.includes(deviceUuid);
}

function hasConnectionSubscribedDeviceAnomalies(connectionId: string, deviceUuid: string): boolean {
    const connection = getConnection(connectionId);
    if (connection === undefined)
        return false;

    return connection.subscribedDeviceAnomalies.includes(deviceUuid);
}

async function handleDisconnectDeviceAction(action: DisconnectDeviceAction) {
    let affectedSensorConnectionIds = subscribedDeviceSensors[action.deviceUuid] ?? [];
    let affectedAnomaliesConnectionIds = subscribedDeviceAnomalies[action.deviceUuid] ?? [];

    if (affectedSensorConnectionIds.length === 0 && affectedAnomaliesConnectionIds.length === 0) {
        console.log(`WARN: we received message to disconnect device ${action.deviceUuid}, but no connections have this device subscribed!`);
        return;
    }

    if (action.userUuids !== undefined) {
        const userUuids = action.userUuids;
        affectedSensorConnectionIds = affectedSensorConnectionIds.filter((id) => userUuids.includes(openConnections[id].userUuid));
        affectedAnomaliesConnectionIds = affectedAnomaliesConnectionIds.filter((id) => userUuids.includes(openConnections[id].userUuid));
    }

    if (affectedSensorConnectionIds.length !== 0) {
        console.log(`Unsubscribing device sensors ${action.deviceUuid} from ${affectedSensorConnectionIds.length} connections, reason ${action.reason}...`);
        await Promise.all(
            affectedSensorConnectionIds.map((id) => unsubscribeSensorsConnectionFromDeviceWithAlert(id, action.deviceUuid, action.reason))
        );
    }

    if (affectedAnomaliesConnectionIds.length !== 0) {
        console.log(`Unsubscribing device anomalies ${action.deviceUuid} from ${affectedSensorConnectionIds.length} connections, reason ${action.reason}...`);
        await Promise.all(
            affectedAnomaliesConnectionIds.map((id) => unsubscribeAnomaliesConnectionFromDeviceWithAlert(id, action.deviceUuid, action.reason))
        );
    }
}

/**
 * Just a function that pools pending connected device actions from redis.
 */
async function poolDeviceActions() {
    const uuids = Object.keys(openConnections);
    if (uuids.length === 0)
        return;

    let payload = await consumeDeviceAction(podName);
    while (payload !== null) {
        const {action} = payload;

        console.log(`Received action ${action}!`);

        switch (action) {
            case "disconnectDevice":
                await handleDisconnectDeviceAction(payload);
                break;
        }

        payload = await consumeDeviceAction(podName);
    }
}

async function subscribeSensorConnectionToDevice(connectionId: string, deviceUuid: string) {
    const connection = getConnection(connectionId);
    if (connection == undefined)
        return;

    //Adding device to redis
    await markDeviceConnected(
        deviceUuid,
        podName,
        connection.userUuid,
        connectionId
    );

    //Adding device to kafka.
    await producer.send({
        topic: deviceSensorSubscriptionTopic,
        messages: [{
            key: deviceUuid,
            value: JSON.stringify({
                action: "add",
                pod: podName
            })
        }]
    });

    connection.subscribedDeviceSensors.push(deviceUuid);

    subscribedDeviceSensors[deviceUuid] = [
        ...(subscribedDeviceSensors[deviceUuid] ?? []),
        connectionId
    ];
}

async function subscribeAnomaliesConnectionToDevice(connectionId: string, deviceUuid: string) {
    const connection = getConnection(connectionId);
    if (connection == undefined)
        return;

    //Adding device to redis
    await markDeviceAnomaliesConnected(
        deviceUuid,
        podName,
        connection.userUuid,
        connectionId
    );

    //Adding device to kafka.
    await producer.send({
        topic: deviceAnomaliesSubscriptionTopic,
        messages: [{
            key: deviceUuid,
            value: JSON.stringify({
                action: "add",
                pod: podName
            })
        }]
    });

    connection.subscribedDeviceAnomalies.push(deviceUuid);

    subscribedDeviceAnomalies[deviceUuid] = [
        ...(subscribedDeviceAnomalies[deviceUuid] ?? []),
        connectionId
    ];
}

async function unsubscribeSensorsConnectionFromDeviceWithAlert(connectionId: string, deviceUuid: string, reason: string | undefined) {
    await sendDeviceSensorsDisconnectedNotificationToUser(connectionId, deviceUuid, reason);
    await unsubscribeSensorConnectionFromDevice(connectionId, deviceUuid);
}

async function unsubscribeSensorConnectionFromDevice(connectionId: string, deviceUuid: string) {
    const connection = getConnection(connectionId);
    if (connection === undefined)
        return;

    //Removing device from redis.
    await markDeviceDisconnected(
        deviceUuid,
        podName,
        connection.userUuid,
        connectionId
    );

    //Removing device from connection state.
    const index = connection.subscribedDeviceSensors.indexOf(deviceUuid);
    if (index === -1)
        return;
    connection.subscribedDeviceSensors.splice(index, 1);

    //Removing device from subscribed devices.
    if (subscribedDeviceSensors[deviceUuid].length === 1) {
        delete subscribedDeviceSensors[deviceUuid];

        //We only want to remove device from kafka in case that there are no more open subscriptions.
        await producer.send({
            topic: deviceSensorSubscriptionTopic,
            messages: [{
                key: deviceUuid,
                value: JSON.stringify({
                    action: "remove",
                    pod: podName
                })
            }]
        });
    } else {
        const index = subscribedDeviceSensors[deviceUuid].indexOf(connectionId);
        if (index === -1)
            return;
        subscribedDeviceSensors[deviceUuid].splice(index, 1);
    }
}

async function unsubscribeAnomaliesConnectionFromDeviceWithAlert(connectionId: string, deviceUuid: string, reason: string | undefined) {
    await sendDeviceAnomaliesDisconnectedNotificationToUser(connectionId, deviceUuid, reason);
    await unsubscribeAnomaliesConnectionFromDevice(connectionId, deviceUuid);
}

async function unsubscribeAnomaliesConnectionFromDevice(connectionId: string, deviceUuid: string) {
    const connection = getConnection(connectionId);
    if (connection === undefined)
        return;

    //Removing device from redis.
    await markDeviceAnomaliesDisconnected(
        deviceUuid,
        podName,
        connection.userUuid,
        connectionId
    );

    //Removing device from connection state.
    const index = connection.subscribedDeviceAnomalies.indexOf(deviceUuid);
    if (index === -1)
        return;
    connection.subscribedDeviceAnomalies.splice(index, 1);

    //Removing device from subscribed devices.
    if (subscribedDeviceAnomalies[deviceUuid].length === 1) {
        delete subscribedDeviceAnomalies[deviceUuid];

        //We only want to remove device from kafka in case that there are no more open subscriptions.
        await producer.send({
            topic: deviceAnomaliesSubscriptionTopic,
            messages: [{
                key: deviceUuid,
                value: JSON.stringify({
                    action: "remove",
                    pod: podName
                })
            }]
        });
    } else {
        const index = subscribedDeviceAnomalies[deviceUuid].indexOf(connectionId);
        if (index === -1)
            return;
        subscribedDeviceAnomalies[deviceUuid].splice(index, 1);
    }
}

function closeConnectionOnTokenExpiration(connectionId: string) {
    const {connection, userUuid} = openConnections[connectionId];

    console.log(`Closing connection with ${userUuid}, as token expired...`);
    connection.close(1002, "Token has expired!");
}

console.log("Connecting to MySQL...");
const prisma = new PrismaClient();

function initHttpServer<
    Request extends typeof http.IncomingMessage = typeof http.IncomingMessage,
    Response extends typeof http.ServerResponse = typeof http.ServerResponse,
>() {
    const requestListener: http.RequestListener<Request, Response> = (request, response) => {
        console.log("Received request for " + request.url);
        response.writeHead(404);
        response.end();
    };

    return http.createServer(requestListener);
}

let httpServer: Server;
let wsServer: WebSocketServer;

async function main() {
    setInterval(() => poolDeviceActions(), poolActionsEveryMs);

    console.log("Creating http server...");
    httpServer = initHttpServer();

    const PORT = process.env.PORT;
    httpServer.listen(PORT, () => {
        console.log(`Server is listening on port ${PORT}`);
    });

    wsServer = new WebSocketServer({
        httpServer: httpServer,
        autoAcceptConnections: false
    });

    wsServer.on('request', handleRequest);

    await mainKafka();
}

async function deleteKafkaTopic() {
    console.log(`Deleting topic ${deviceSensorTopic}...`);
    const admin = kafka.admin();
    await admin.connect();
    await admin.deleteTopics({
        topics: [deviceSensorTopic, deviceAnomaliesTopic]
    });
    await admin.disconnect();
}

function terminate() {
    console.log("All connections have been terminated... Setting up interval to check when all resources from redis will be released!")

    const interval = setInterval(() => {
        if (Object.keys(openConnections).length !== 0)
            return;

        console.log("There is no anymore open connections...");
        clearInterval(interval);

        deleteKafkaTopic().then(() => {
            console.log("Exiting...")
            process.exit(0);
        });
    }, 1000);
}

process.on('SIGTERM', () => {
    console.log("Got SIGTERM. Graceful shutdown start");

    console.log("Shutting down http server...");
    httpServer.close(terminate);

    console.log("Shutting down ws server...");
    wsServer.closeAllConnections();
    wsServer.shutDown();
});

async function handleRequest(request: request) {
    const userToken = findUserTokenFromRequest(request);

    if (userToken === null) {
        console.log("Client tried connecting null token!");
        request.reject();
        return;
    }

    const decoded = verifyToken(userToken);
    if (decoded === null) {
        console.log("Client tried connecting with an invalid token!")
        request.reject();
        return;
    }

    const userUuid = decoded.user.uuid;

    if (!await markUserConnected(userUuid)) {
        console.log("Client tried opening too much simultaneous websocket connections!");
        request.reject();
        return;
    }

    const connectionId = generateNewConnectionId();

    openConnections[connectionId] = {
        userUuid: userUuid,
        connection: request.accept('', request.origin),
        guardTimeout: setTimeout(
            () => closeConnectionOnTokenExpiration(connectionId),
            calcMillisBeforeTokenExpires(decoded)
        ),
        subscribedDeviceSensors: [],
        subscribedDeviceAnomalies: []
    };

    //Ok now connection is established.
    handleAcceptedConnection(connectionId);
}

function handleAcceptedConnection(connectionId: string) {
    const openConnection = getConnection(connectionId);
    if (openConnection === undefined)
        return;

    const {connection, userUuid} = openConnection;
    console.log(`User connected: ${userUuid}`);

    connection.on('message', async (message) => {
        if (message.type === 'binary') {
            console.log(`Connection from user ${userUuid} closing, because it send binary message.`);
            connection.close(1002, "Binary messages are not supported!");
            return;
        }

        if (message.type === 'utf8') {
            console.log(`Received message from user ${userUuid}.`);
            console.log(`Parsing message json.`)
            try {
                await handleMessage(JSON.parse(message.utf8Data), connectionId);
            } catch (e) {
                console.log("User didn't send a valid json!")
                connection.close(1002, "Sent message was not a JSON!");
            }
        }
    });

    connection.on(
        'close',
        (...args) => onConnectionClosed(connectionId, ...args)
    );
}

async function handleSubscribeSensorsMessage(payload: any, connectionId: string) {
    const connection = getConnection(connectionId);
    if (connection === undefined)
        return;

    const {deviceId: deviceUuid} = payload;
    const {userUuid} = connection;

    console.log(`${userUuid} asked to subscribe device sensors ${deviceUuid}...`)

    if (hasConnectionSubscribedDeviceSensors(connectionId, deviceUuid)) {
        console.log(`${userUuid} already has this device sensors subscribed!`)
        return;
    }

    const deviceOwnedByUser = await doesDeviceBelongToUser(prisma, deviceUuid, userUuid);
    if (!deviceOwnedByUser) {
        console.log(`Device ${deviceUuid} does not belong to user ${userUuid}!`);
        return;
    }

    await subscribeSensorConnectionToDevice(connectionId, deviceUuid);
}

async function handleUnsubscribeSensorsMessage(payload: any, connectionId: string) {
    const connection = getConnection(connectionId);
    if (connection === undefined)
        return;

    const {deviceId: deviceUuid} = payload;
    const {userUuid} = connection;

    console.log(`${userUuid} asked to unsubscribe device sensors ${deviceUuid}...`)

    if (!hasConnectionSubscribedDeviceSensors(connectionId, deviceUuid)) {
        console.log(`${userUuid} does not have this device sensors subscribed!`)
        return;
    }

    await unsubscribeSensorConnectionFromDevice(connectionId, deviceUuid);
}

async function handleSubscribeAnomaliesMessage(payload: any, connectionId: string) {
    const connection = getConnection(connectionId);
    if (connection === undefined)
        return;

    const {deviceId: deviceUuid} = payload;
    const {userUuid} = connection;

    console.log(`${userUuid} asked to subscribe device anomalies ${deviceUuid}...`)

    if (hasConnectionSubscribedDeviceAnomalies(connectionId, deviceUuid)) {
        console.log(`${userUuid} already has this device anomalies subscribed!`)
        return;
    }

    const deviceOwnedByUser = await doesDeviceBelongToUser(prisma, deviceUuid, userUuid);
    if (!deviceOwnedByUser) {
        console.log(`Device ${deviceUuid} does not belong to user ${userUuid}!`);
        return;
    }

    await subscribeAnomaliesConnectionToDevice(connectionId, deviceUuid);
}

async function handleUnsubscribeAnomaliesMessage(payload: any, connectionId: string) {
    const connection = getConnection(connectionId);
    if (connection === undefined)
        return;

    const {deviceId: deviceUuid} = payload;
    const {userUuid} = connection;

    console.log(`${userUuid} asked to unsubscribe device anomalies ${deviceUuid}...`)

    if (!hasConnectionSubscribedDeviceAnomalies(connectionId, deviceUuid)) {
        console.log(`${userUuid} does not have this device anomalies subscribed!`)
        return;
    }

    await unsubscribeAnomaliesConnectionFromDevice(connectionId, deviceUuid);
}

function handleUpdateTokenMessage(payload: any, connectionId: string) {
    const connection = getConnection(connectionId);
    if (connection === undefined)
        return;

    const {token} = payload;
    const {userUuid, guardTimeout} = connection;
    console.log(`${userUuid} asked to refresh his access token...`)

    const decoded = verifyToken(token);
    if (decoded === null) {
        console.log(`Client ${userUuid} tried to refresh an invalid token!`)
        console.log("Token will not be refreshed!");
        return;
    }

    if (decoded.user.uuid !== userUuid) {
        console.log(`Client ${userUuid} tried to refresh token, which belongs to different user ${decoded.user.uuid}!`)
        console.log("Token will not be refreshed!");
        return;
    }

    clearTimeout(guardTimeout);
    openConnections[connectionId] = {
        ...openConnections[connectionId],
        guardTimeout: setTimeout(
            () => closeConnectionOnTokenExpiration(connectionId),
            calcMillisBeforeTokenExpires(decoded)
        ),
    }

    console.log("Token has been refreshed!");
}

function handleDefaultMessage(payloadType: string, connectionId: string) {
    const connection = getConnection(connectionId);
    if (connection === undefined)
        return;

    console.log(`${connection.userUuid} sent an unknown message type: ${payloadType}`);
    connection.connection.close(1002, `Unknown message type ${payloadType}}`);
    return;
}

async function handleMessage(payload: any, connectionId: string) {
    switch (payload.type) {
        case "subscribe":
            await handleSubscribeSensorsMessage(payload, connectionId);
            break;
        case "unsubscribe":
            await handleUnsubscribeSensorsMessage(payload, connectionId);
            break;
        case "subscribeAnomalies":
            await handleSubscribeAnomaliesMessage(payload, connectionId);
            break;
        case "unsubscribeAnomalies":
            await handleUnsubscribeAnomaliesMessage(payload, connectionId);
            break;
        case "updateToken":
            handleUpdateTokenMessage(payload, connectionId);
            break;
        default:
            handleDefaultMessage(payload.type, connectionId);
            break;
    }
}

async function onConnectionClosed(connectionId: string, reasonCode: number, description: string) {
    const connection = getConnection(connectionId);
    if (connection === undefined)
        return;

    const subscribedDevicesSensorCopy = [...connection.subscribedDeviceSensors];
    await Promise.all(subscribedDevicesSensorCopy.map(async (deviceUuid) => unsubscribeSensorConnectionFromDevice(connectionId, deviceUuid)));

    const subscribedDevicesAnomaliesCopy = [...connection.subscribedDeviceAnomalies];
    await Promise.all(subscribedDevicesAnomaliesCopy.map(async (deviceUuid) => unsubscribeAnomaliesConnectionFromDevice(connectionId, deviceUuid)));

    await markUserDisconnected(connection.userUuid);

    console.log(`Connection from user ${connection.userUuid} closed. Reason code: ${reasonCode}, Description ${description}`);
    clearTimeout(connection.guardTimeout);
    delete openConnections[connectionId];
}

function findUserTokenFromRequest(request: request): string | null {
    const urlString = request.httpRequest.url;
    if (urlString === undefined)
        return null;

    const url = new URL(appUrl + urlString);
    return url.searchParams.get('token');
}

async function mainKafka() {
    const admin = kafka.admin();

    console.log(`Creating topic ${deviceSensorTopic}...`);
    await admin.connect();
    await admin.createTopics({
        topics: [
            {
                topic: deviceSensorTopic
            },
            {
                topic: deviceAnomaliesTopic
            }
        ]
    });
    await admin.disconnect();

    await consumer.subscribe({topics: [deviceSensorTopic, deviceAnomaliesTopic]});

    console.log("Starting consumer...");
    await consumer.run({
        eachMessage: async ({message: {offset, value, key}, topic}: EachMessagePayload) => {
            console.log(`Consumed ${topic} offset: ${offset}...`);

            switch (topic) {
                case deviceSensorTopic:
                    handleDeviceSensorMessage(key, value, offset);
                    break;
                case deviceAnomaliesTopic:
                    handleDeviceAnomalyMessage(value, offset);
                    break;
                default:
                    console.log(`Received messages from unknown topic ${topic}`);
                    return;
            }
        }
    });
}

function handleDeviceSensorMessage(key: Buffer | null, value: Buffer | null, offset: string) {
    if (value === null) {
        console.log("Value is null, skipping!");
        return;
    }

    if (key === null) {
        console.log("Key is null, skipping!");
        return;
    }

    const keyPayload = JSON.parse(key.toString());
    const deviceUuid = keyPayload.deviceKey;
    const timestamp = moment(keyPayload.date).valueOf();
    const subscribedUsers = subscribedDeviceSensors[deviceUuid];
    if (subscribedUsers === undefined) {
        console.log(`Received device sensor message for ${deviceUuid}, but no users have it subscribed!`);
        return;
    }

    const payload = JSON.parse(JSON.parse(value.toString()).json);

    console.log(`Sending sensor offset ${offset} to ${subscribedUsers.length} users...`);
    subscribedUsers.forEach((connectionId) => sendSensorMessageToUser(connectionId, deviceUuid, payload, timestamp))
}

function handleDeviceAnomalyMessage(value: Buffer | null, offset: string) {
    if (value === null) {
        console.log("Value is null, skipping!");
        return;
    }

    const {deviceUuid, date} = JSON.parse(value.toString());
    const subscribedUsers = subscribedDeviceAnomalies[deviceUuid];
    if (subscribedUsers === undefined) {
        console.log(`Received device anomaly message for ${deviceUuid}, but no users have it subscribed!`);
        return;
    }

    const timestamp = moment(date).valueOf();

    console.log(`Sending anomaly offset ${offset} to ${subscribedUsers.length} users...`);
    subscribedUsers.forEach((connectionId) => sendAnomalyMessageToUser(connectionId, deviceUuid, timestamp));
}

function sendDeviceSensorsDisconnectedNotificationToUser(connectionId: string, deviceUuid: string, reason: string | undefined) {
    console.log(`Sending a device sensors disconnected message to connection ${connectionId}...`);

    const connInfo = getConnection(connectionId);
    if (connInfo === undefined) {
        console.warn(`No open connection was found for ${connectionId}!`);
        return;
    }

    connInfo.connection.sendUTF(JSON.stringify({
        type: "deviceDisconnected",
        deviceUuid: deviceUuid,
        reason: reason,
    }));
}

function sendDeviceAnomaliesDisconnectedNotificationToUser(connectionId: string, deviceUuid: string, reason: string | undefined) {
    console.log(`Sending a device anomalies disconnected message to connection ${connectionId}...`);

    const connInfo = getConnection(connectionId);
    if (connInfo === undefined) {
        console.warn(`No open connection was found for ${connectionId}!`);
        return;
    }

    connInfo.connection.sendUTF(JSON.stringify({
        type: "deviceAnomaliesDisconnected",
        deviceUuid: deviceUuid,
        reason: reason,
    }));
}

function sendAnomalyMessageToUser(connectionId: string, deviceUuid: string, timestampMs: number) {
    console.log(`Sending a data message to connection ${connectionId}...`);

    const connInfo = getConnection(connectionId);
    if (connInfo === undefined) {
        console.warn(`No open connection was found for ${connectionId}!`);
        return;
    }

    connInfo.connection.sendUTF(JSON.stringify({
        type: "anomaly",
        deviceUuid: deviceUuid,
        timestampMs: timestampMs
    }));
}

function sendSensorMessageToUser(connectionId: string, deviceUuid: string, payload: any, timestampMs: number) {
    console.log(`Sending a data message to connection ${connectionId}...`);

    const connInfo = getConnection(connectionId);
    if (connInfo === undefined) {
        console.warn(`No open connection was found for ${connectionId}!`);
        return;
    }

    connInfo.connection.sendUTF(JSON.stringify({
        type: "data",
        deviceUuid: deviceUuid,
        payload: payload,
        timestampMs: timestampMs
    }));
}

main()
    .then(async () => {
        await prisma.$disconnect();
    })
    .catch(async (e) => {
        console.error(e);
        await prisma.$disconnect();
        process.exit(1);
    });