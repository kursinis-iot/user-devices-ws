import {PrismaClient} from "@prisma/client";
import {EachMessagePayload} from "kafkajs";

import consumer from "../services/kafka/consumer.js";
import {deleteDevice, upsertDeviceWithUsers, UserData} from "../repositories/device.js";
import {
    getAllPodNamesFromWhichDeviceIsSubscribed,
    getAllPodNamesFromWhichDeviceIsSubscribedByUsers
} from "../services/redis/device.js";
import {dispatchDeviceAction} from "../services/redis/deviceActions.js";
import {
    getAllPodNamesFromWhichDeviceAnomaliesIsSubscribed, getAllPodNamesFromWhichDeviceAnomaliesIsSubscribedByUsers
} from "../services/redis/deviceAnomalies.js";

const devicesTopic = process.env.DEVICES_TOPIC ?? '';

//TODO: add more roles.
export type DeviceUserRole = "owner";

export type DeviceUserPayload = {
    role: DeviceUserRole
    created_at: string,
    updated_at: string,
}

export type DeviceMessagePayload = {
    uuid: string,
    created_at: string,
    updated_at: string,
    users: Record<string, DeviceUserPayload | null>
}

console.log("Connecting to MySQL...");
const prisma = new PrismaClient();

async function main() {
    await consumer.subscribe({topic: devicesTopic, fromBeginning: true});

    console.log("Starting consumer...");
    await consumer.run({
        eachMessage: async ({message: {offset, key, value}}: EachMessagePayload) => {
            console.log(`Consumed offset: ${offset}...`);
            if (key === null) {
                console.log(`Key is null, skipping...`);
                return;
            }

            const deviceUuid = key.toString();

            if (value === null) {
                console.log(`Value is null, deleting ${deviceUuid} device!`);
                await deleteDevice(prisma, deviceUuid);

                //Here we dispatch action to websockets to disconnect this device.
                const podsToUnsubscribe = new Set([
                    ...await getAllPodNamesFromWhichDeviceIsSubscribed(deviceUuid),
                    ...await getAllPodNamesFromWhichDeviceAnomaliesIsSubscribed(deviceUuid)
                ]);
                if (podsToUnsubscribe.size === 0)
                    return;

                console.log(`Device is connected from ${podsToUnsubscribe.size} connections, dispatching disconnect actions...!`);
                await Promise.all(
                    [...podsToUnsubscribe].map((pod) => dispatchDeviceAction(pod, {
                        action: "disconnectDevice",
                        deviceUuid: deviceUuid,
                        userUuids: undefined,
                        reason: "Device has been removed!"
                    }))
                );

                return;
            }

            const data: DeviceMessagePayload = JSON.parse(value.toString()) as any;

            console.log(`Writing new data for device (${deviceUuid}) to database...`);
            const usersToRemove = Object.entries(data.users).reduce<string[]>(
                (agg, [userUuid, pivot]) => {
                    if (pivot === null)
                        agg.push(userUuid);

                    return agg;
                },
                []
            );

            const userData = Object.entries(data.users).reduce<UserData[]>(
                (agg, [userUuid, pivot]) => {
                    if (pivot !== null)
                        agg.push({
                            uuid: userUuid,
                            role: pivot.role
                        });

                    return agg;
                },
                []
            );

            await upsertDeviceWithUsers(prisma, deviceUuid, userData, usersToRemove);
            console.log(`Device entry (${deviceUuid}) has been updated!`);

            //Here we dispatch action to websockets to disconnect this device (only for usersToRemove).
            const podsToUnsubscribe = new Set([
                ...await getAllPodNamesFromWhichDeviceIsSubscribedByUsers(deviceUuid, usersToRemove),
                ...await getAllPodNamesFromWhichDeviceAnomaliesIsSubscribedByUsers(deviceUuid, usersToRemove)
            ]);
            if (podsToUnsubscribe.size === 0)
                return;

            console.log(`There is ${podsToUnsubscribe.size} device subscriptions to be terminated, dispatching actions...`);
            await Promise.all(
                [...podsToUnsubscribe].map((pod) => dispatchDeviceAction(pod, {
                    action: "disconnectDevice",
                    deviceUuid: deviceUuid,
                    userUuids: usersToRemove,
                    reason: "Device relation to user has been removed!"
                }))
            );
        }
    });
}

main()
    .then(async () => {
        await prisma.$disconnect();
    })
    .catch(async (e) => {
        console.error(e);
        await prisma.$disconnect();
        process.exit(1);
    });
