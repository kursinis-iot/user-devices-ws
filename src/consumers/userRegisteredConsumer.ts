import {PrismaClient} from '@prisma/client'
import {EachMessagePayload} from "kafkajs";

import consumer from "../services/kafka/consumer.js";
import {deleteUser, upsertUser} from "../repositories/user.js";

const userRegisteredTopic = process.env.USER_REGISTERED_TOPIC ?? '';

console.log("Connecting to MySQL...");
const prisma = new PrismaClient();

async function main() {
    await consumer.subscribe({topic: userRegisteredTopic, fromBeginning: true});

    console.log("Starting consumer...");
    await consumer.run({
        eachMessage: async ({message: {offset, value, key}}: EachMessagePayload) => {
            console.log(`Consumed offset: ${offset}...`);
            if(key === null) {
                console.log("Key is null, skipping.");
                return;
            }

            const uuid = key.toString();
            if (value === null) {
                console.log(`Received user ${uuid} deleted message!`);

                console.log("Deleting user entry from database...");
                await deleteUser(prisma, uuid);
                console.log("User entry has been deleted!");
                return;
            }

            const payload = JSON.parse(value.toString());
            const {email, first_name, last_name, language} = payload;
            console.log(`Received user ${uuid} upserted message!`);

            console.log("Upserting user entry to database...");
            await upsertUser(prisma, uuid, email, first_name, last_name, language ?? 'lt');
            console.log("User entry has been upserted!");
        }
    });
}

main()
    .then(async () => {
        await prisma.$disconnect();
    })
    .catch(async (e) => {
        console.error(e);
        await prisma.$disconnect();
        process.exit(1);
    });