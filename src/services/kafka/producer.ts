import kafka from "./kafka.js";

console.log("Creating Kafka Producer...");
const producer = kafka.producer();
await producer.connect();

export default producer;
export async function dispose() {
    return producer.disconnect();
}