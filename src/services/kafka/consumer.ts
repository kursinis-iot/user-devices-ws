import kafka from "./kafka.js";
import * as crypto from "crypto";

function getGroupId(): string {
    const groupId = process.env.KAFKA_GROUP_ID ?? 'default';
    if ((process.env.KAFKA_RANDOM_GROUP_ID_POSTFIX ?? "") === "true")
        return groupId + "-" + crypto.randomBytes(16).toString('hex');

    return groupId;
}

const consumer = kafka.consumer({groupId: getGroupId()});
await consumer.connect();

export default consumer;