import jwt from "jsonwebtoken";

const jwtPublicKey = process.env.JWT_PUBLIC_KEY ?? '';

export function verifyToken(token: string): DecodedToken | null {
    try {
        const {payload} = jwt.verify(token, jwtPublicKey, {
            algorithms: ['RS512'],
            complete: true
        });

        return payload as DecodedToken;
    } catch (e) {
        return null;
    }
}

export type DecodedTokenUser = {
    uuid: string
}

export type DecodedToken = {
    user: DecodedTokenUser
    exp: number,
}

export function calcMillisBeforeTokenExpires({exp: tokenExpiresAt}: DecodedToken): number {
    return (tokenExpiresAt * 1000) - Date.now();
}