import client, {prefix} from "./client.js";

export function constructDeviceAnomaliesKey(uuid: string): string {
    return `${prefix}.deviceAnomalies.${uuid}`;
}

export type DeviceAnomaliesConnectedPayload = {
    podName: string,
    userUuid: string,
    connectionId: string
}

function serializeDeviceAnomaliesConnectedPayload(
    payload: DeviceAnomaliesConnectedPayload
): string {
    return JSON.stringify(payload);
}

function deserializeDeviceAnomaliesConnectedPayload(
    string: string
): DeviceAnomaliesConnectedPayload {
    return JSON.parse(string) as DeviceAnomaliesConnectedPayload;
}

export async function markDeviceAnomaliesConnected(
    uuid: string,
    podName: string,
    userUuid: string,
    connectionId: string
) {
    await client.sAdd(
        constructDeviceAnomaliesKey(uuid),
        serializeDeviceAnomaliesConnectedPayload({
            podName: podName,
            userUuid: userUuid,
            connectionId: connectionId
        })
    );
}

export async function markDeviceAnomaliesDisconnected(
    uuid: string,
    podName: string,
    userUuid: string,
    connectionId: string
) {
    await client.sRem(
        constructDeviceAnomaliesKey(uuid),
        serializeDeviceAnomaliesConnectedPayload({
            podName: podName,
            userUuid: userUuid,
            connectionId: connectionId
        })
    )
}

export async function getAllDeviceAnomaliesConnectedPayloads(
    uuid: string
): Promise<DeviceAnomaliesConnectedPayload[]> {
    const payloads = await client.sMembers(constructDeviceAnomaliesKey(uuid));

    return payloads.map((member: string) => deserializeDeviceAnomaliesConnectedPayload(member));
}

export async function getAllPodNamesFromWhichDeviceAnomaliesIsSubscribed(
    uuid: string
): Promise<string[]> {
    const payloads = await getAllDeviceAnomaliesConnectedPayloads(uuid);
    const result = new Set<string>();

    payloads.forEach(subscription => result.add(subscription.podName));

    return [...result];
}

export async function getAllPodNamesFromWhichDeviceAnomaliesIsSubscribedByUsers(
    deviceUuid: string,
    userUuids: string[]
): Promise<string[]> {
    const payloads = await getAllDeviceAnomaliesConnectedPayloads(deviceUuid);
    const result = new Set<string>();

    payloads.filter((payload) => userUuids.includes(payload.userUuid))
        .forEach(payload => result.add(payload.podName));

    return [...result];
}