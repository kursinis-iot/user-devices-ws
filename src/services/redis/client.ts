import {createClient} from "redis";

export const prefix = "user-devices-ws";

const redis = await createClient({
    url: process.env.REDIS_URL,
    database: process.env.REDIS_DATABASE ?? 0
})
    .on('error', (err: any) => console.log('Redis client error', err))
    .connect();

export default redis;