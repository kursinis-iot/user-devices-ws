import client, {prefix} from "./client.js";

export function constructDeviceKey(uuid: string): string {
    return `${prefix}.devices.${uuid}`;
}

export type DeviceConnectedPayload = {
    podName: string,
    userUuid: string,
    connectionId: string
}

function serializeDeviceConnectedPayload(
    payload: DeviceConnectedPayload
): string {
    return JSON.stringify(payload);
}

function deserializeDeviceConnectedPayload(
    string: string
): DeviceConnectedPayload {
    return JSON.parse(string) as DeviceConnectedPayload;
}

export async function markDeviceConnected(
    uuid: string,
    podName: string,
    userUuid: string,
    connectionId: string
) {
    await client.sAdd(
        constructDeviceKey(uuid),
        serializeDeviceConnectedPayload({
            podName: podName,
            userUuid: userUuid,
            connectionId: connectionId
        })
    );
}

export async function markDeviceDisconnected(
    uuid: string,
    podName: string,
    userUuid: string,
    connectionId: string
) {
    await client.sRem(
        constructDeviceKey(uuid),
        serializeDeviceConnectedPayload({
            podName: podName,
            userUuid: userUuid,
            connectionId: connectionId
        })
    )
}

export async function getAllDeviceConnectedPayloads(
    uuid: string
): Promise<DeviceConnectedPayload[]> {
    const payloads = await client.sMembers(constructDeviceKey(uuid));

    return payloads.map((member: string) => deserializeDeviceConnectedPayload(member));
}

export async function getAllPodNamesFromWhichDeviceIsSubscribed(
    uuid: string
): Promise<string[]> {
    const payloads = await getAllDeviceConnectedPayloads(uuid);
    const result = new Set<string>();

    payloads.forEach(subscription => result.add(subscription.podName));

    return [...result];
}

export async function getAllPodNamesFromWhichDeviceIsSubscribedByUsers(
    deviceUuid: string,
    userUuids: string[]
): Promise<string[]> {
    const payloads = await getAllDeviceConnectedPayloads(deviceUuid);
    const result = new Set<string>();

    payloads.filter((payload) => userUuids.includes(payload.userUuid))
        .forEach(payload => result.add(payload.podName));

    return [...result];
}