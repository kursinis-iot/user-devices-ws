import client, {prefix} from "./client.js";

export type DisconnectDeviceAction = {
    action: "disconnectDevice",
    deviceUuid: string
    userUuids: string[] | undefined //If user uuids is undefined, then we must disconnect from all users.
    reason?: string
}

export type DeviceAction = DisconnectDeviceAction;

export function constructDeviceActionQueueKey(pod: string): string {
    return `${prefix}.actions.${pod}`;
}

export async function dispatchDeviceAction(
    pod: string,
    action: DeviceAction
): Promise<void> {
    await client.rPush(
        constructDeviceActionQueueKey(pod),
        JSON.stringify(action)
    );
}

export async function consumeDeviceAction(
    pod: string
): Promise<DeviceAction | null> {
    const val = await client.lPop(constructDeviceActionQueueKey(pod));
    if (val === null)
        return null;

    return JSON.parse(val);
}