import client from "./client.js";

const maxUserConnections = 10;

export function constructUserConnectionCountKey(uuid: string) {
    return `users.${uuid}.connectionCount`;
}

export async function markUserConnected(
    uuid: string
): Promise<boolean> {
    const key = constructUserConnectionCountKey(uuid);
    const sessionCount = await client.incr(key);

    if (sessionCount > maxUserConnections) {
        await client.decr(key);
        return false;
    }

    return true;
}

export async function markUserDisconnected(
    uuid: string
) {
    const key = constructUserConnectionCountKey(uuid);
    await client.decr(key);
}