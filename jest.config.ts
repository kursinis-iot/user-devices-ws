import type {Config} from 'jest';

const config: Config = {
    preset: 'ts-jest/presets/default-esm',
    testEnvironment: 'node',
    moduleNameMapper: {
        "(.+)\\.js": "$1"
    },
    setupFiles: ['dotenv/config']
};

export default config;