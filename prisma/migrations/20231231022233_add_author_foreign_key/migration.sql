-- AddForeignKey
ALTER TABLE `Device` ADD CONSTRAINT `Device_authorUuid_fkey` FOREIGN KEY (`authorUuid`) REFERENCES `User`(`uuid`) ON DELETE CASCADE ON UPDATE CASCADE;
