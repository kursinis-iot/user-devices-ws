-- DropForeignKey
ALTER TABLE `Device` DROP FOREIGN KEY `Device_authorUuid_fkey`;

-- DropForeignKey
ALTER TABLE `DeviceUser` DROP FOREIGN KEY `DeviceUser_deviceUuid_fkey`;

-- DropForeignKey
ALTER TABLE `DeviceUser` DROP FOREIGN KEY `DeviceUser_userUuid_fkey`;

-- AlterTable
ALTER TABLE `Device` MODIFY `authorUuid` CHAR(36) NULL;

-- AlterTable
ALTER TABLE `User` ADD COLUMN `language` CHAR(2) NOT NULL DEFAULT 'lt';

-- AddForeignKey
ALTER TABLE `Device` ADD CONSTRAINT `Device_authorUuid_fkey` FOREIGN KEY (`authorUuid`) REFERENCES `User`(`uuid`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `DeviceUser` ADD CONSTRAINT `DeviceUser_deviceUuid_fkey` FOREIGN KEY (`deviceUuid`) REFERENCES `Device`(`uuid`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `DeviceUser` ADD CONSTRAINT `DeviceUser_userUuid_fkey` FOREIGN KEY (`userUuid`) REFERENCES `User`(`uuid`) ON DELETE CASCADE ON UPDATE CASCADE;
