/*
  Warnings:

  - Added the required column `role` to the `DeviceUser` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `DeviceUser` ADD COLUMN `role` VARCHAR(255) NOT NULL;
