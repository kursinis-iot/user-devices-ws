/*
  Warnings:

  - You are about to drop the column `authorUuid` on the `Device` table. All the data in the column will be lost.

*/
-- DropForeignKey
ALTER TABLE `Device` DROP FOREIGN KEY `Device_authorUuid_fkey`;

-- AlterTable
ALTER TABLE `Device` DROP COLUMN `authorUuid`;
