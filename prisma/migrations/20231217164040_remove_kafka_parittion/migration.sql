/*
  Warnings:

  - You are about to drop the column `kafkaSensorPartition` on the `Device` table. All the data in the column will be lost.

*/
-- DropIndex
DROP INDEX `Device_kafkaSensorPartition_key` ON `Device`;

-- AlterTable
ALTER TABLE `Device` DROP COLUMN `kafkaSensorPartition`;
