import client from "../../../src/services/redis/client";
import {
    getAllPodNamesFromWhichDeviceIsSubscribed,
    getAllPodNamesFromWhichDeviceIsSubscribedByUsers,
    markDeviceConnected,
    markDeviceDisconnected
} from "../../../src/services/redis/device";

afterEach(async () => {
    await client.flushAll();
});

afterAll(async () => {
    await client.disconnect()
});

test("getAllPodNamesFromWhichDeviceIsSubscribed tests", async () => {
    expect(await getAllPodNamesFromWhichDeviceIsSubscribed("test")).toEqual([]);

    await markDeviceConnected("test", "pod-a", "user1", "1");
    await markDeviceConnected("test", "pod-b", "user1", "2");
    await markDeviceConnected("test2", "pod-c", "user1", "3");

    expect(await getAllPodNamesFromWhichDeviceIsSubscribed("test")).toEqual(['pod-a', 'pod-b']);

    await markDeviceDisconnected("test", "pod-b", "user1", "2");
    await markDeviceDisconnected("test2", "pod-c", "user1", "3");

    expect(await getAllPodNamesFromWhichDeviceIsSubscribed("test")).toEqual(['pod-a']);
});

test("getAllPodNamesFromWhichDeviceIsSubscribedByUsers tests", async () => {
    expect(await getAllPodNamesFromWhichDeviceIsSubscribedByUsers("test", ["user1", "user3"])).toEqual([]);

    await markDeviceConnected("test", "pod-a", "user1", "1");
    await markDeviceConnected("test", "pod-b", "user1", "2");
    await markDeviceConnected("test2", "pod-c", "user1", "3");
    await markDeviceConnected("test", "pod-d", "user2", "4");
    await markDeviceConnected("test", "pod-e", "user3", "5");

    expect(await getAllPodNamesFromWhichDeviceIsSubscribedByUsers("test", ["user1", "user3"])).toEqual(['pod-a', 'pod-b', 'pod-e']);

    await markDeviceDisconnected("test", "pod-b", "user1", "2");
    await markDeviceDisconnected("test2", "pod-c", "user1", "3");
    await markDeviceDisconnected("test", "pod-e", "user3", "5");

    expect(await getAllPodNamesFromWhichDeviceIsSubscribedByUsers("test", ["user1", "user3"])).toEqual(['pod-a']);
});