import client from "../../../src/services/redis/client";
import {
    constructUserConnectionCountKey,
    markUserConnected,
    markUserDisconnected
} from "../../../src/services/redis/users";

afterEach(async () => {
    await client.flushAll();
});

afterAll(async () => {
    await client.disconnect()
});

test("markUserConnected should only work 10 times", async () => {
    for (let i = 0; i < 10; i++)
        expect(await markUserConnected("test")).toBe(true);

    expect(await markUserConnected("test")).toBe(false);
});

test("markUserConnected should increase the counter by 1", async () => {
    const key = constructUserConnectionCountKey("test");
    await client.set(key, 5);
    expect(await markUserConnected("test")).toBe(true);
    expect(await client.get(key)).toBe("6");
});

test("markUserDisconnected should decrease the counter by 1", async () => {
    const key = constructUserConnectionCountKey("test");
    await client.set(key, 5);
    await markUserDisconnected("test");
    expect(await client.get(key)).toBe("4");
});
