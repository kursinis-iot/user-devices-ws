import client from "../../../src/services/redis/client";
import {
    consumeDeviceAction,
    DisconnectDeviceAction,
    dispatchDeviceAction
} from "../../../src/services/redis/deviceActions";

afterEach(async () => {
    await client.flushAll();
});

afterAll(async () => {
    await client.disconnect()
});

const disconnectAction: DisconnectDeviceAction = {
    action: "disconnectDevice",
    deviceUuid: "test1",
    userUuids: undefined
};

const disconnectAction2: DisconnectDeviceAction = {
    action: "disconnectDevice",
    deviceUuid: "test2",
    userUuids: ["test", "test1"]
};

test("integration test", async () => {
    expect(await consumeDeviceAction("a")).toBeNull();
    expect(await consumeDeviceAction("b")).toBeNull();

    await dispatchDeviceAction("a", disconnectAction2);
    expect(await consumeDeviceAction("a")).toEqual(disconnectAction2);
    expect(await consumeDeviceAction("a")).toBeNull();
    expect(await consumeDeviceAction("b")).toBeNull();

    await dispatchDeviceAction("a", disconnectAction2);
    await dispatchDeviceAction("a", disconnectAction);
    await dispatchDeviceAction("b", disconnectAction2);
    expect(await consumeDeviceAction("a")).toEqual(disconnectAction2);
    expect(await consumeDeviceAction("a")).toEqual(disconnectAction);
    expect(await consumeDeviceAction("a")).toBeNull();
    expect(await consumeDeviceAction("b")).toEqual(disconnectAction2);
    expect(await consumeDeviceAction("b")).toBeNull();
});