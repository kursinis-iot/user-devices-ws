import client from "../../../src/services/redis/client";
import {
    getAllPodNamesFromWhichDeviceAnomaliesIsSubscribed, 
    getAllPodNamesFromWhichDeviceAnomaliesIsSubscribedByUsers,
    markDeviceAnomaliesConnected,
    markDeviceAnomaliesDisconnected
} from "../../../src/services/redis/deviceAnomalies";

afterEach(async () => {
    await client.flushAll();
});

afterAll(async () => {
    await client.disconnect()
});

test("getAllPodNamesFromWhichDeviceAnomaliesIsSubscribed tests", async () => {
    expect(await getAllPodNamesFromWhichDeviceAnomaliesIsSubscribed("test")).toEqual([]);

    await markDeviceAnomaliesConnected("test", "pod-a", "user1", "1");
    await markDeviceAnomaliesConnected("test", "pod-b", "user1", "2");
    await markDeviceAnomaliesConnected("test2", "pod-c", "user1", "3");

    expect(await getAllPodNamesFromWhichDeviceAnomaliesIsSubscribed("test")).toEqual(['pod-a', 'pod-b']);

    await markDeviceAnomaliesDisconnected("test", "pod-b", "user1", "2");
    await markDeviceAnomaliesDisconnected("test2", "pod-c", "user1", "3");

    expect(await getAllPodNamesFromWhichDeviceAnomaliesIsSubscribed("test")).toEqual(['pod-a']);
});

test("getAllPodNamesFromWhichDeviceAnomaliesIsSubscribedByUsers tests", async () => {
    expect(await getAllPodNamesFromWhichDeviceAnomaliesIsSubscribedByUsers("test", ["user1", "user3"])).toEqual([]);

    await markDeviceAnomaliesConnected("test", "pod-a", "user1", "1");
    await markDeviceAnomaliesConnected("test", "pod-b", "user1", "2");
    await markDeviceAnomaliesConnected("test2", "pod-c", "user1", "3");
    await markDeviceAnomaliesConnected("test", "pod-d", "user2", "4");
    await markDeviceAnomaliesConnected("test", "pod-e", "user3", "5");

    expect(await getAllPodNamesFromWhichDeviceAnomaliesIsSubscribedByUsers("test", ["user1", "user3"])).toEqual(['pod-a', 'pod-b', 'pod-e']);

    await markDeviceAnomaliesDisconnected("test", "pod-b", "user1", "2");
    await markDeviceAnomaliesDisconnected("test2", "pod-c", "user1", "3");
    await markDeviceAnomaliesDisconnected("test", "pod-e", "user3", "5");

    expect(await getAllPodNamesFromWhichDeviceAnomaliesIsSubscribedByUsers("test", ["user1", "user3"])).toEqual(['pod-a']);
});